# SIP automated calling with audio playback

Use case: We want to ring an old analogue phone during a Halloween party. When
the phone is picked up, an audio file should play.

## Hardware used in this project
- Fritz!box 7340
- Raspberry Pi 1
- Old, Dutch bakelite phone (we had to replace an internal capacitor to make
  it work again) or some other phone connected to the Fritz!box

## How to
### Preparation
- On Raspbian, install the `sipp` tool via `apt install sip-tester`.
- Prepare an audio file, as explained on [StackOverflow](https://stackoverflow.com/a/40489510):
  - Open your audio file in [Audacity](https://www.audacityteam.org/)
  - Convert it to mono (Tracks -> Mix -> Mix stereo down to mono)
  - In the bottom left corner, set the Project Rate to 8000
  - Save the file via File -> Export -> Export audio.... Set the format in the
    bottom right corner to 'Other compressed files'. Set the header to 'Wav
    (Microsoft)' and the encoding to 'A-Law'. Save your file with the `.wav`
    extension.
- Replace the existing `wav` filename in the `<exec rtp_stream="7days.wav,-1" />` 
  line in the `scenario.xml` file from this repository.
- In the Fritz!box, add a new telephony device.
  - In the web interface, go to Telephony -> Telephony Devices
  - Click on Configure New Device
  - Select 'Telephone (with or without answering machine)'
  - Choose 'LAN/WLAN (IP telephone)'. The name does not matter.
  - You now get a user name and you can enter a password. In my case I got user
    name 623 and thus I use password 623.
  - Select some number for outgoing calls.
  - I chose to accept no incoming calls.

### Calling
On the Raspberry pi, you can start a call with the following command:
```
sipp -sf scenario.xml  192.168.178.1  -s **610 -i 192.168.178.22 -au 620 -ap 620 -aa -m 1 -l 1 -d 100000 -r 1 -users 1 -default_behaviors -abortunexp
```
- The first `192.168.178.1` should be the IP address of the Fritz!box.
- `**610` is the number of the phone you want to call. Look for the internal
  number in the table on the 'Telephony Devices' page of the Fritz!box. 
- `192.168.178.22` represents the IP of the Raspberry Pi. You can replace this
  part with `$(/sibn/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)`
  to automatically determine this IP (assuming you are connected via the
  Raspberry's ethernet port and that interface has only one IPv4 address
  assigned to it).
- `-a 620 -ap 620` specifies the username and password that was set up on the
  Fritz!box.
- The rest of the flags might not be needed. They try to get `sipp` to only
  start one call at a time. In the current situation it tries to start a call
  every second until the phone gets picked up. 

#### From Node-RED
Use the `exec` node to start the included `call.sh` script (edit the parameters
inside the script first). When a call is already being made, the script will do
nothing.

## Alternatives
I've tried `pjsua` but it gave me some problems:
- The audio file was started at the moment you start the call. That means that
  when you leave the phone ringing for 10 seconds that you'll miss the first 10
  seconds of the audio file. That was unacceptable in my use case.
- The audio started to stutter after approximately 1 minute (when running
  `pjsua` on my desktop with an Intel Core i7 CPU).
- It used a fair amount of CPU (20% on my Intel) so it might not even run that
  well on the Raspberry Pi 1. This is probably due to the flexibility of
  `pjsua`: It converts the input `wav` file to a suitable format on-the-fly.
  The CPU usage of `ssip` is just a few percent on the Raspberry Pi 1.

## General remarks

Please do not use this to hurt people. Stalking people is bad, mkay. 
